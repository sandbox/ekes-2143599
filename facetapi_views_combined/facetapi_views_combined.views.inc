<?php

/**
 * @file
 * Views API base implementation.
 */

/**
 * Implements hook_views_plugins().
 */
function facetapi_views_combined_views_plugins() {
  return array(
    'query' => array(
      'facetapi_views_data_query' => array(
        'title' => t('Combined FacetAPI Data Query'),
        'help' => t('Gives access to FacetAPI data from the views realm.'),
        'handler' => 'facetapi_views_data_query',
      ),
    ),
  );
}

/**
 * Implements hook_views_data().
 */
function facetapi_views_combined_views_data() {
  $data = array();
  // Create a base for each enabled search.
  $searchers = facetapi_get_searcher_info();
  foreach ($searchers as $search_name => $searcher) {
    $table_name = 'facetapi_views_combined__' . $search_name;
    $data[$table_name]['table']['group'] = t('Facets');
    $data[$table_name]['table']['base'] = array(
      'query class' => 'facetapi_views_data_query',
      'title' => t('@search Facets - Combined', array('@search' => $searcher['label'])),
      'description' => t('Combined Facet API fields.'),
    );
    // And fields for each enabled facet for the search.
    $facets = facetapi_get_enabled_facets($search_name, 'views_data');
    foreach ($facets as $facet_name => $facet) {
      foreach ($facet['query types'] as $query_type) {
        if ($query_type == 'term') {
          $data[$table_name][$facet_name . ':markup'] = array(
            'title' => t('@facet - Term', array('@facet' => $facet['label'])),
            'help' => t('Basic term value.'),
            'field' => array(
              'handler' => 'facetapi_views_handler_field',
              'click sortable' => TRUE,
            ),
            'sort' => array(
              'handler' => 'facetapi_views_handler_sort',
            ),
          );
        }
        elseif ($query_type == 'date') {
          $data[$table_name][$facet_name . ':date_start'] = array(
            'title' => t('@facet - Date', array('@facet' => $facet['label'])),
            'help' => t('Date facet value'),
            'field' => array(
              'handler' => 'facetapi_views_handler_field_date',
              'click sortable' => TRUE,
            ),
            'sort' => array(
              'handler' => 'facetapi_views_handler_sort',
            ),
          );
        }
      }
      $data[$table_name][$facet_name . ':path'] = array(
        'title' => t('@facet - Path', array('@facet' => $facet['label'])),
        'help' => t('Path for facet filter.'),
        'field' => array(
          'handler' => 'facetapi_views_handler_field_url',
          'click sortable' => TRUE,
        ),
        'sort' => array(
          'handler' => 'facetapi_views_handler_sort',
        ),
      );
      $data[$table_name][$facet_name . ':count'] = array(
        'title' => t('@facet - Count', array('@facet' => $facet['label'])),
        'help' => t('Count for the facet.'),
        'field' => array(
          'handler' => 'facetapi_views_handler_field_numeric',
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'facetapi_views_handler_filter_count',
        ),
        'sort' => array(
          'handler' => 'facetapi_views_handler_sort',
        ),
        'argument' => array(
          'handler' => 'facetapi_views_handler_argument',
        ),
      );
      $data[$table_name][$facet_name . ':active'] = array(
        'title' => t('@facet - Status', array('@facet' => $facet['label'])),
        'help' => t('If the facet is selected, active.'),
        'field' => array(
          'handler' => 'facetapi_views_handler_field_boolean',
          'click sortable' => TRUE,
        ),
        'sort' => array(
          'handler' => 'facetapi_views_handler_sort',
        ),
      );
      $data[$table_name][$facet_name . ':indexed_value'] = array(
        'title' => t('@facet - Indexed value', array('@facet' => $facet['label'])),
        'help' => t('Raw indexed value. Can be string or often a id or key.'),
        'field' => array(
          'handler' => 'facetapi_views_handler_field',
        ),
      );
      $data[$table_name][$facet_name . ':item_parents'] = array(
        'title' => t('@facet - Parents', array('@facet' => $facet['label'])),
        'help' => t('Any parents the facet has. Values are the indexed value.'),
        'field' => array(
          'handler' => 'facetapi_views_handler_field_multiple',
        ),
      );
      $data[$table_name][$facet_name . ':item_children'] = array(
        'title' => t('@facet - Children', array('@facet' => $facet['label'])),
        'help' => t('Any children the facet has. Values are the indexed value.'),
        'field' => array(
          'handler' => 'facetapi_views_handler_field_multiple',
        ),
      );
    }
  }

  return $data;
}
