<?php

/**
 * Views base table query class for data from Facet API.
 */

/**
 * Views Plugin Query retrieves data from Facet API.
 */
class facetapi_views_data_query extends views_plugin_query {

  /**
   * The searcher that this facet is working on.
   *
   * @var searcher_name
   */
  public $searcher_name;

  /**
   * The realm for the view data facets.
   *
   * @var realm_name
   */
  public $realm_name;

  /**
   * {@inheritdoc}
   */
  public function init($base_table, $base_field, $options) {
    parent::init($base_table, $base_field, $options);
    if (substr($base_table, 0, 25) == 'facetapi_views_combined__') {
      $this->searcher_name = substr($base_table, 25);
      // Only one realm for now, fixing.
      $this->realm_name = 'views_data';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function execute(&$view) {
    $adapter = facetapi_adapter_load($this->searcher_name);
    $facets = $adapter->buildRealm($this->realm_name);
    foreach ($facets as $id => $facet) {
      if ($id[0] == '#') {
        continue;
      }
      $type = $facet['#facet']['field type'];
      foreach ($facet[$id] as $value) {
        $this->buildFacetRow($view, $value, $id, $type);
      }
    }
  }

  /**
   * Recursive function to get facet data from render array, for views row.
   */
  public function buildFacetRow(&$view, $value, $id, $type) {
    $children = array();
    $row = new stdClass();
    $row->{$id . ':count'} = $value['#count'];
    // Views doesn't handle the first /.
    $row->{$id . ':path'} = substr(url($value['#path'], array('query' => $value['#query'])), 1);
    $row->{$id . ':active'} = $value['#active'];
    $row->{$id . ':markup'} = $value['#markup'];
    // Not yet exposed. Later will need to be multi-value. What's the
    // format? @todo
    $row->{$id . ':indexed_value'} = $value['#indexed_value'];
    if (!empty($value['#item_children'])) {
      $row->{$id . ':item_children'} = array();
      foreach ($value['#item_children'] as $child_indexed_value => $child) {
        $this->buildFacetRow($view, $child, $id, $type);
        $row->{$id . ':item_children'}[] = $child_indexed_value;
      }
    }
    if (is_array($value['#item_parents'])) {
      $row->{$id . ':item_parents'} = array_keys($value['#item_parents']);
    }
    // Special row handling.
    if ($type == 'date') {
      // We should/could use the query type plugin. However search_api
      // doesn't implement extract seperately, at present. So manually
      // converting date from standard.
      $regex = str_replace(array('^', '$'), '', FACETAPI_REGEX_DATE);
      preg_match($regex, $value['#indexed_value'], $matches);
      if (isset($matches[0])) {
        // Only handling start date at the moment.
        $row->{$id . ':date_start'} = strtotime($matches[0]);
      }
    }
    $view->result[] = $row;
  }

  /**
   * {@inheritdoc}
   */
  public function query($get_count = FALSE) {
  }

  /**
   * Implementation of sql add_field.
   *
   * Should only be needed while we're reusing sql field plugins.
   */
  public function add_field($table, $field, $alias = '', $params = array()) {
    return $alias ? $alias : $field;
  }
}
