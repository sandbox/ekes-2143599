<?php

/**
 * @file
 * Simple Facet API widget that uses a view to display data.
 */

/**
 * Widget that uses views to display output.
 */
class FacetAPIViewsDataWidget extends FacetapiWidget {

  /**
   * {@inheritdoc}
   */
  public function execute() {
    // Can we trigger this from the view data layer?
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultSettings() {
    return array();
  }

  /**
   * Overrides FacetapiWidget::settingsForm().
   *
   * Add option to set view and display.
   */
  public function settingsForm(&$form, &$form_state) {
  }
}
