<?php

/**
 * @file
 * Basic handler for displaying FacetAPI fields.
 */

/**
 * Stripped down version of Views url handler.
 */
class facetapi_views_handler_field_url extends views_handler_field_url {

  /**
   * {@inheritdoc}
   */
  public function query($group_by = FALSE) {
    // @todo If relationships with multiple tables does this need an alias.
    $this->field_alias = $this->real_field;
  }

  /**
   * No table to ensure.
   */
  public function ensure_my_table() {}

  /**
   * Sort.
   *
   * @todo
   */
  public function click_sort($order) {
  }
}
