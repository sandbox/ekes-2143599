<?php

/**
 * @file
 * Basic handler for displaying FacetAPI date fields.
 */

/**
 * Stripped down version of Views date handler.
 */
class facetapi_views_handler_field_date extends views_handler_field_date {

  /**
   * {@inheritdoc}
   */
  public function query($group_by = FALSE) {
    // @todo If relationships with multiple tables does this need an alias.
    $this->field_alias = $this->real_field;
  }

  /**
   * No table to ensure.
   */
  public function ensure_my_table() {}

  /**
   * Sort.
   *
   * @todo
   */
  public function click_sort($order) {
  }
}
