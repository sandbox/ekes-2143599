<?php

/**
 * @file
 * Basic handler for displaying FacetAPI fields.
 */

/**
 * Stripped down version of Views basic handler.
 */
class facetapi_views_handler_field extends views_handler_field {
  /**
   * {@inheritdoc}
   */
  public function query($group_by = FALSE) {
    // @todo If relationships with multiple tables does this need an alias.
    $this->field_alias = $this->real_field;
  }

  /**
   * No table to ensure.
   */
  public function ensure_my_table() {}

  /**
   * Sort.
   *
   * @todo
   */
  public function click_sort($order) {
  }
}

/**
 * Basic field with enabled multiple handling.
 */
class facetapi_views_handler_field_multiple extends facetapi_views_handler_field {
  /**
   * Does the field supports multiple field values.
   *
   * @var bool
   */
  public $multiple;

  /**
   * {@inheritdoc}
   */
  public function init(&$view, &$options) {
    parent::init($view, $options);
    $this->multiple = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function option_definition() {
    $options = parent::option_definition();

    // Options used for multiple value fields.
    $options['group_rows'] = array(
      'default' => TRUE,
      'bool' => TRUE,
    );
    $options['delta_limit'] = array(
      'default' => 'all',
    );
    $options['delta_offset'] = array(
      'default' => 0,
    );
    $options['delta_reversed'] = array(
      'default' => FALSE,
      'bool' => TRUE,
    );
    $options['delta_first_last'] = array(
      'default' => FALSE,
      'bool' => TRUE,
    );

    $options['multi_type'] = array(
      'default' => 'separator',
    );
    $options['separator'] = array(
      'default' => ', ',
    );

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    // If this is a multiple value field, add its options.
    if ($this->multiple) {
      $this->multiple_options_form($form, $form_state);
    }
  }

  /**
   * Provide options for multiple value fields.
   */
  function multiple_options_form(&$form, &$form_state) {

    $form['multiple_field_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Multiple field settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#weight' => 5,
    );

    $form['group_rows'] = array(
      '#title' => t('Display all values in the same row'),
      '#type' => 'checkbox',
      '#default_value' => $this->options['group_rows'],
      '#description' => t('If checked, multiple values for this field will be shown in the same row. If not checked, each value in this field will create a new row. If using group by, please make sure to group by "Entity ID" for this setting to have any effect.'),
      '#fieldset' => 'multiple_field_settings',
    );

    // Make the string translatable by keeping it as a whole rather than
    // translating prefix and suffix separately.
    list($prefix, $suffix) = explode('@count', t('Display @count value(s)'));

    $type = 'textfield';
    $options = NULL;
    $size = 5;

    $form['multi_type'] = array(
      '#type' => 'radios',
      '#title' => t('Display type'),
      '#options' => array(
        'ul' => t('Unordered list'),
        'ol' => t('Ordered list'),
        'separator' => t('Simple separator'),
      ),
      '#dependency' => array('edit-options-group-rows' => array(TRUE)),
      '#default_value' => $this->options['multi_type'],
      '#fieldset' => 'multiple_field_settings',
    );

    $form['separator'] = array(
      '#type' => 'textfield',
      '#title' => t('Separator'),
      '#default_value' => $this->options['separator'],
      '#dependency' => array(
        'radio:options[multi_type]' => array('separator'),
        'edit-options-group-rows' => array(TRUE),
      ),
      '#dependency_count' => 2,
      '#fieldset' => 'multiple_field_settings',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query($group_by = FALSE) {
    // @todo If relationships with multiple tables does this need an alias.
    $this->field_alias = $this->real_field;
  }

  /**
   * Return render array of items.
   */
  function get_items($values) {
    $items = array();
    if (!empty($values->{$this->options['id']})) {
      foreach ($values->{$this->options['id']} as $value) {
        $items[] = array('#markup' => $value);
      }
    }
    return $items;
  }

  /**
   * Render a single item.
   */
  public function render_item($count, $item) {
    return $item;
  }

  /**
   * {@inheritdoc}
   */
  public function render_items($items) {
    if (!empty($items)) {
      if ($this->options['multi_type'] == 'separator') {
        return implode(filter_xss_admin($this->options['separator']), $items);
      }
      else {
        return theme('item_list',
          array(
            'items' => $items,
            'title' => NULL,
            'type' => $this->options['multi_type'],
          ));
      }
    }
  }
}
