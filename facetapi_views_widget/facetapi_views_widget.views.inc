<?php

/**
 * @file
 * Views API base implementation.
 */

/**
 * Implements hook_views_plugins().
 */
function facetapi_views_widget_views_plugins() {
  return array(
    'query' => array(
      'facetapi_views_widget_query' => array(
        'title' => t('FacetAPI Query'),
        'help' => t('Gives access to FacetAPI data.'),
        'handler' => 'facetapi_views_widget_query',
      ),
    ),
  );
}

/**
 * Implements hook_views_data().
 */
function facetapi_views_widget_views_data() {
  // Do a manual example first. Then move over to pulling the facets
  // from the facetapi; and relating them to field implementations
  // from either apachesolr_views or search_api.
  // Search api views additionally has the code for iterating over the
  // potential facets.
  $data = array();
  $data['facetapi_views_widget']['table']['group'] = t('Facets');
  $data['facetapi_views_widget']['table']['base'] = array(
    'query class' => 'facetapi_views_widget_query',
    'title' => t('Facets: Simple'),
    'description' => t('Basic facet field types.'),
  );

  $data['facetapi_views_widget']['string'] = array(
    'title' => t('Term'),
    'help' => t('Basic term value'),
    'field' => array(
      'handler' => 'facetapi_views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'facetapi_views_handler_sort',
    ),
  );
  $data['facetapi_views_widget']['date_start'] = array(
    'title' => t('Date'),
    'help' => t('Date facet value'),
    'field' => array(
      'handler' => 'facetapi_views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'facetapi_views_handler_sort',
    ),
  );
  $data['facetapi_views_widget']['path'] = array(
    'title' => t('Term path'),
    'help' => t('Path for term filter.'),
    'field' => array(
      'handler' => 'facetapi_views_handler_field_url',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'facetapi_views_handler_sort',
    ),
  );
  $data['facetapi_views_widget']['count'] = array(
    'title' => t('Term count'),
    'help' => t('Count for term facets'),
    'field' => array(
      'handler' => 'facetapi_views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'facetapi_views_handler_filter_count',
    ),
    'sort' => array(
      'handler' => 'facetapi_views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'facetapi_views_handler_argument',
    ),
  );
  $data['facetapi_views_widget']['active'] = array(
    'title' => t('Term status'),
    'help' => t('If the term is selected, active.'),
    'field' => array(
      'handler' => 'facetapi_views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'facetapi_views_handler_sort',
    ),
  );

  return $data;
}
