<?php

/**
 * Views base table query class for data from Facet API.
 */

/**
 * Views Plugin Query retrieves data from Facet API.
 */
class facetapi_views_widget_query extends views_plugin_query {

  /**
   * {@inheritdoc}
   */
  public function execute(&$view) {
    if (empty($view->facetapi_data)) {
      return;
    }
    // To get the data we either need it injected by the caller.
    // Or we need to get it from a custom realm object.
    // For the simple facetapi block realm use case, the block widget
    // pushes the data into the view.
    $data = $view->facetapi_data[$view->facetapi_field];
    $query_types_plugins = $view->facetapi_data['#adapter']->loadQueryTypePlugins();
    foreach ($data as $value) {
      $row = new stdClass();
      // Handle basic native query types.
      foreach ($view->facetapi_data['#facet']['query types'] as $type) {
        if ($type == 'term') {
          $row->string = $value['#markup'];
        }
        elseif ($type == 'date') {
          // We should/could use the query type plugin. However search_api
          // doesn't implement extract seperately, at present. So manually
          // converting date from standard.
          $regex = str_replace(array('^', '$'), '', FACETAPI_REGEX_DATE);
          preg_match($regex, $value['#indexed_value'], $matches);
          if (isset($matches[0])) {
            // Only handling start date at the moment.
            $row->date_start = strtotime($matches[0]);
          }
        }
      }
      // @todo add plugin system to allow 'field' data to be added specific for
      // modules. (date module type fields, taxonomy, other entity etc.)
      $row->count = $value['#count'];
      // Views doesn't handle the first /.
      $row->path = substr(url($value['#path'], array('query' => $value['#query'])), 1);
      $row->active = $value['#active'];
      $view->result[] = $row;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function query($get_count = FALSE) {}
}
