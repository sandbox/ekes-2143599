<?php

/**
 * @file
 * Simple Facet API widget that uses a view to display data.
 */

/**
 * Widget that uses views to display output.
 */
class FacetAPIViewsWidget extends FacetapiWidget {

  /**
   * {@inheritdoc}
   */
  public function execute() {
    list($view_name, $display_name) = explode(':', $this->settings->settings['view']);
    // Load view.
    $view = views_get_view($view_name);
    // Add data from facet.
    $view->facetapi_data = &$this->build;
    $view->facetapi_field = $this->facet['field alias'];
    // Generate output for display.
    $output = $view->preview($display_name);
    // Pass as markup to facet block.
    $this->build[$this->facet['field alias']] = array(
      '#type' => 'markup',
      '#markup' => $output,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultSettings() {
    return array(
      // The default view that is with the module.
      'view' => 'facetapi_views_widget_default:default',
    );
  }

  /**
   * Overrides FacetapiWidget::settingsForm().
   *
   * Add option to set view and display.
   */
  public function settingsForm(&$form, &$form_state) {
    $views = views_get_enabled_views();
    $options = array();
    foreach ($views as $view) {
      if ($view->base_table != 'facetapi_views_widget') {
        continue;
      }
      foreach ($view->display as $display_id => $display) {
        $options[$view->name . ':' . $display_id] = t('@view : @display', array('@view' => $view->get_human_name(), '@display' => $display->id));
      }
    }
    $form['widget']['widget_settings']['views_widget'][$this->id]['view'] = array(
      '#type' => 'radios',
      '#title' => t('View'),
      '#description' => t('Which view should be used to display the widget data.'),
      '#default_value' => $this->settings->settings['view'],
      '#options' => $options,
      '#states' => array(
        'visible' => array(
          'select[name="widget"]' => array('value' => $this->id),
        ),
      ),
    );
  }
}
