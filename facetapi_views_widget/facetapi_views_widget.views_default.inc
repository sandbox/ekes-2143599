<?php

/**
 * @file
 * Default view for displaying a facet.
 */

/**
 * Implements hook_views_default_views().
 */
function facetapi_views_widget_views_default_views() {
  $views = array();

  $view = new view();
  $view->name = 'facetapi_views_widget_default';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'facetapi_views_widget';
  $view->human_name = 'FacetAPI widget simple';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE;

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Facets: Term */
  $handler->display->display_options['fields']['string']['id'] = 'string';
  $handler->display->display_options['fields']['string']['table'] = 'facetapi_views_widget';
  $handler->display->display_options['fields']['string']['field'] = 'string';
  /* Field: Facets: Date */
  $handler->display->display_options['fields']['date_start']['id'] = 'date_start';
  $handler->display->display_options['fields']['date_start']['table'] = 'facetapi_views_widget';
  $handler->display->display_options['fields']['date_start']['field'] = 'date_start';
  $handler->display->display_options['fields']['date_start']['date_format'] = 'long';
  /* Field: Facets: Term path */
  $handler->display->display_options['fields']['path']['id'] = 'path';
  $handler->display->display_options['fields']['path']['table'] = 'facetapi_views_widget';
  $handler->display->display_options['fields']['path']['field'] = 'path';
  /* Field: Facets: Term count */
  $handler->display->display_options['fields']['count']['id'] = 'count';
  $handler->display->display_options['fields']['count']['table'] = 'facetapi_views_widget';
  $handler->display->display_options['fields']['count']['field'] = 'count';
  $handler->display->display_options['fields']['count']['separator'] = '';
  /* Field: Facets: Term status */
  $handler->display->display_options['fields']['active']['id'] = 'active';
  $handler->display->display_options['fields']['active']['table'] = 'facetapi_views_widget';
  $handler->display->display_options['fields']['active']['field'] = 'active';
  $handler->display->display_options['fields']['active']['label'] = 'Active';
  $handler->display->display_options['fields']['active']['not'] = 0;

  $views[$view->name] = $view;

  return $views;
}
